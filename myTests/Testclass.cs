﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
//using Calculator;


namespace myTests
{
    // Magdeli Holmøy Asplin
    // 8/29/2019

    // This class contains the tests for checking that the calculator does what it is supposed to do. It contains
    // six separate tests

    public class Testclass
    {
        Calculator calculator = new Calculator();
        
        [Fact]
        public void Add_ShouldAddCorrectly()

        {
            // This tests checks that the result is correct when using the add method

            // Arrange
            double expected = 8;

            // Act
            double actual = calculator.Add(4, 4);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Subtract_ShouldSubtractCorrectly()
        {
            // This test checks that the result is correct when using the subtract method

            // Arrange
            double expected = 0;

            // Act
            double actual = calculator.Subtract(4, 4);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Subtract_ShouldMultiplyCorrectly()
        {
            // This test checks that the result is correct when using the multiply method

            // Arrange
            double expected = 16;

            // Act
            double actual = calculator.Multiply(4, 4);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Subtract_ShouldDivideCorrectly()
        {
            // This test checks that the result is correct when using the divide method

            // Arrange
            double expected = 1;

            // Act
            double actual = calculator.Divide(4.0, 4.0);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void Divide_NotDivideByZero()
        {
            // This test checks if the divide method throws an exception when trying to divide by zero

            // Arrange


            // Act
            Action act = () => calculator.Divide(10, 0);

            // Assert
            Assert.Throws<ArgumentException>(act);


        }

        [Fact]
        public void Add_AddsIntandDouble()
        {
            // This test checks that the add method works when trying to add an integer and a double

            // Arrange
            double expected = 10.2;

            // Act
            double actual = calculator.Add(10, 0.2);

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}
