﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace myTests
{
    // Magdeli Holmøy Asplin
    // 8/29/2019

    // This class is a calculator which can add, subtract, divide and multiply

    public class Calculator
    {

        #region Attributes
        #endregion


        #region Constructors

        public Calculator() { }

        #endregion


        #region Behaviours

        public double Add(double num1, double num2)
        {
            // The method for adding
            double answer = num1 + num2;
            return answer;
        }

        public double Subtract(double num1, double num2)
        {
            // The method for subtracting
            double answer = num1 - num2;
            return answer;
        }

        public double Multiply(double num1, double num2)
        {
            // The method for multiplying
            double answer = num1 * num2;
            return answer;
        }

        public double Divide(double num1, double num2)
        {
            // The method for dividing, which throws an exception if trying to divide by zero
            if (num2 == 0)
            {
                throw new ArgumentException();
            }
            else
            {
                double answer = num1 / num2;
                return answer;
            }
        }

        #endregion
    }
}
